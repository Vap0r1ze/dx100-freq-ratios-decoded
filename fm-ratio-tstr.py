# fm-ratio-tstr.py
# Python code to generate Addendum: √2, √3, π/4 and π to 30x multiplication as part of the DX100 FREQ RATIOS DECODED repository
# u can use this code to generate new charts to higher resolution or larger multiplication factors for DX7 et al with larger FM ratios
# Dave Burraston June 2021
# www.noyzelab.com

a = 0.71 # root2/2
b = 1.41 # roo2
c = 0.87 # root3/2
d = 1.73 # root3
e = 0.785 # pi/4
f = 3.14 # pi
sigfigs = 3
multiplier = 31

print("root2", "\t root3", "\t pi/4", "\t pi")
for n in range(1,multiplier):
    print(round(b*n, sigfigs),   "\t", round(d*n, sigfigs), "\t", round(e*n, sigfigs), "\t", round(f*n, sigfigs))
    
