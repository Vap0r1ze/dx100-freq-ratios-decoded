**DX100 FREQUENCY RATIO CHART DECODED & ORGANISED**

The Yamaha DX100 owners manual contains a very interesting chart of its "carefully chosen" frequency ratios, although there is barely any discussion about them at all or what they actually are or relate to.. 

These mysterious ratios also appear in the other 4, 6 & 8 operator Yamaha FM synthesizers/chips, such as the DX7, DX21, TX81Z, FB-01, DX11, FS1R, DEXED, you name it.. so the information here is relevant for any FM synthesizer capable of producing these ratios. 

With a little deciphering using a tiny Python program it turns out this mysterious chart contains 4 groups of inharmonic ratios : √2, √3, π/4 and π. Included in this repo are => 

**- spreadsheet in Open Office & PDF format deciphering and organising these ratios into a more coherent layout, plus an Addendum of complete tables computed up to 30x multiplication**

**- Python program to produce the Addendum tables. Edit & rerun this code to generate output for higher ratios, for example alter the multiplier variable to print out higher ratios for use with 6 & 8 operator synths.**

**- excerpt from the original DX100 owners manual showing these ratios obtained from here =>** https://usa.yamaha.com/products/contents/music_production/downloads/manuals/index.html?k=&c=music_production&l=en&p=5

Yamaha have been quite cunning, as you will see from the tables I've made decoding this mysterious chart. The inclusion of these 4 inharmonic ratio groups has been done in such a way that they contain exact multiples of themselves. This is incredibly useful when programming an FM sound using just sine waves, and as Yamaha hint they "produce extremely complex waveforms" for things like "sound effects including extremely realistic bells, explosions, etc." 

Below is a simple example I setup and tried on my DX7ii :

Consider creating a sound containing inharmonic timbres, where you would like to have a complex wave modulating a simple sine wave carrier tracking the keyboard normally. This could easily be achieved with a 3 operator sine stack 3>2>1. For the carrier [operator 1] you could choose a ratio of 1.0. Then you could setup operator 2 at √2 = 1.41 creating an inharmonic ratio, and modulate it with operator 3 using √2 but at x 2 = 2.82. This would give you a 2:1 ratio for operators 3 & 2 respectively and create a nice complex modulating wave with minimal to no beating. You could further adjust either or both operator 2 & 3 ratios using √2 as the inharmonic ratio to keep it whole number [integer] based.. or move across to another inharmonic set for one or both and explore futher from there.

**how to keep me going ==>**
if u find this repo useful please think about supporting my work either thru my bandcamp page:
https://noyzelab.bandcamp.com/
or send a Paypal donation or otherwise get in touch via noyzelab [at] gmail [dot] com
thanks, dave

